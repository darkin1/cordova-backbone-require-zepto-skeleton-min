#Szkielet JS (mobile i desktop) z wykorzystaniem takich blibiotek jak:

- cordova
- require JS
- zepto JS
- backbone JS
- underscore JS
- hammer JS
- store JS
- mustache JS
- bootstrap
- FB SDK without Phonegap plugin (only JS)



**1.**
==============

aby dało sie zminimalizowac pliki do pojedynczego pliku .js

potrzeba zainstalowac node.js

na stronie:

http://www.webdeveasy.com/optimize-requirejs-projects/


**2.**
==============

w app.build.js ustawiamy konfiguracje

na stronie podane troche info o konfiguracji

http://www.webdeveasy.com/optimize-requirejs-projects/


**3.**
==============

w CMD przechodzimy do katalogi build i odpalamy build.bat


**4.**
==============

W index.html zamiast

<script data-src="js/main" src="js/libs/require.js"></script>

dodajemy zminimalizowany plik z katalogu js/min

<script src="js/min/scripts.min.js"></script>


**A.**
==============

Dodatkowe stronki z informacją o konfiguracji

https://github.com/jrburke/r.js/blob/master/build/example.build.js#L27-35

http://www.youtube.com/watch?v=m6VNhqKDM4E

http://requirejs.org/docs/optimization.html

https://github.com/CaryLandholt/Tutorial-RequireJS-Optimizer

https://github.com/jrburke/r.js/tree/master/lib/rhino


**B.**
==============

dla FB ustawic AppID  w pliku

- js/libs/cordova_fb.js
- js/main.js

w celu sprawdzenia połączenia do FB, po włączeniu apki, można wywołać

FB.login()

FB.api('me', function(x){console.log(x)});
