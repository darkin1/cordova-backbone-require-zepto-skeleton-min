define([
	'jquery',
	'backbone',
	'underscore',
	'views/home'
],
function($, Backbone, _){
	var Router = Backbone.Router.extend({

		initialize: function() {

			Backbone.history.start();

			Backbone.history.bind('route', function(router, route, params) {
				// console.log(router);
			});
		},
		routes: {
			''				: 'home',
			'home'			: 'home'
		},

		'home': function() {

			var view = new App.View.Home({el: $('#content')});
			this.render(view);

		},



		// renderowanie widoku i usuniecie poprzedniego
		// mozliwosc przekazania obiektu z parametrem do funkcji renderujacej
		render: function(view, params) {
			params = params || {};
			//Close the current view

			if (this.currentView) {
				$('#content').empty();

				this.currentView.undelegateEvents();
				delete this.currentView;
			}

			view.render(params);

			//Set the current view
			this.currentView = view;

		return this;
		}

	});

	return Router;
});
