define([
		'jquery',
		'backbone',
		'underscore',
		'models/model',
		'text!templates/home.html',
		'mustache',
		'iscroll'
],
function($, Backbone, _, model, template, Mustache){

	App.View.Home = Backbone.View.extend({

		initialize: function(){
			this.model = new model({
				message: 'Hello World'
			});

		},

		render: function() {

			this.template = Mustache.to_html(template, {
				"modelTmpl" :  this.model.toJSON()
			});

			$(this.el).html( this.template );
		}
	});

	return App.View.Home;
});
