define([
	'jquery',
	'backbone',
	'underscore'

], function($, Backbone, _){
	App.Model.First = Backbone.Model.extend({
		defaults: {

		},

		initialize: function() {

		}
	});

	// App.ModelObj.First = new App.Model.First();
	return App.Model.First;
});
