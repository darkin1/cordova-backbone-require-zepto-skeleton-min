({

    baseUrl: "../",
	name: 'main',
	mainConfigFile: '../main.js',
	fileExclusionRegExp: /^(r|build)\.js$/,
	optimize: "uglify",
	//optimize: "none",	
	out: '../min/scripts.min.js',
	//findNestedDependencies: true,
	
	//optimizeCss: "standard.keepLines",
	//cssIn: "../css/style.css",
    //out: "../css/style-min.css",
	
	
    paths: {
        'requireLibs'		: "libs/require",
    },
	include: 'requireLibs'
	
	


})