// This set's up the module paths for underscore and backbone
require.config({
	'paths': {
		"jquery"		: "libs/zepto.min",//or jquery
		"underscore"	: "libs/underscore-min",
		"backbone"		: "libs/backbone-min",
		"bootstrap"		: "libs/bootstrap.min",
		"iscroll"		: "libs/iscroll-4",
		"hammerjs"		: "libs/hammer.min",
		"jhammerjs"		: "libs/jquery.hammer.min",
		"storejs"		: "libs/store_json2.min",
		'mustache'		: "libs/mustache",
		'cordova_fb'	: "libs/cordova_fb"
	},
	'shim':
	{
		jquery: {
			'exports': '$'
		},
		backbone: {
			'deps': ['underscore', 'jquery'],
			'exports': 'Backbone'
		},
		underscore: {
			'exports': '_'
		}
	}
});

require([
	'jquery',
	'underscore',
	'backbone',
	'app',
	'cordova_fb'
	],
	function($, _, Backbone, app){

	App = _.extend(App, app);
	window.App = App;
	App.init();


	function onDeviceReady(desktop) {
		console.info('#onDeviceReady');

		// jesli mobile
		if (desktop !== true) {
			cordova.exec(null, null, 'SplashScreen', 'hide', []);


			/////////////////////////////////////////
			// napisanie funkcji FB dla cordovy //
			/////////////////////////////////////////

			FBc.getLoginStatus();
			FBc.login();


		}else{
			window.fbAsyncInit = function() {
				FB.init({
					appId  : '000000000000000',// YOUR APP ID
					status : true
				});
				$(document).trigger('FB:inited');
			}
		}
	}


	if (App.Helpers.isMobile()) {
		// This is running on a device so waiting for deviceready event
		document.addEventListener('deviceready', onDeviceReady, false);
	} else {
		// On desktop don't have to wait for anything
		onDeviceReady(true);
	}
});
