FBc = {

  // YOUR APP ID
  my_client_id : '000000000000000',

  // LEAVE THIS
  my_redirect_uri : "https://www.facebook.com/connect/login_success.html",

  // LEAVE THIS
  my_display : "touch",

  // Login Permissions
  my_scope: {scope : 'email,user_birthday,publish_stream'},

  // redirect site from facebook settings
  my_next: 'http://google.com',


  // override function FB.getLoginStatus
  getLoginStatus: function() {

      FB.getLoginStatus = function(callback, force) {
        var data = FB.getAuthResponse();
        if(data && data.accessToken && data.expiresAt && data.expiresAt > (new Date()/1000)) {
          callback({status: "connected"});
        }else{
          callback({status: "unknown"});
        }
      };

  },

  // override function FB.login
  login: function() {
    var that = this;

    FB.login = function(callback, scope) {

      // override scope and function
      if(typeof callback == 'undefined') callback = $.noop;
      if(typeof scope == 'undefined') scope = that.my_scope;


      var authorize_url = "https://www.facebook.com/dialog/oauth?";
      authorize_url += "client_id=" + that.my_client_id;
      authorize_url += "&redirect_uri=" + that.my_redirect_uri;
      authorize_url += "&display=" + that.my_display;
      authorize_url += "&scope="+scope.scope+"&response_type=token";

      //CALL IN APP BROWSER WITH THE LINK
      var ref = window.open(authorize_url, '_blank', 'location=no');

      ref.addEventListener('loadstart', function(event) {

        var loc = event.url;
        var m = loc.match('#access_token=([^&]+)&expires_in=([0-9]+)');
          if(m && m[1]) {
            //CLOSE INAPPBROWSER AND NAVIGATE TO INDEX
            ref.close();

            FB.init({
                appId  : that.my_client_id,
                status : true,
                authResponse: {
                  accessToken: m[1],
                  expiresAt: parseInt(m[2],10)+(new Date()/1000)
                }
              });
            $(document).trigger('FB:inited');
            if(typeof callback == 'function') callback();
          }
      });
    };
  },

  logout: function(callback) {

    if(typeof callback == 'undefined') callback = $.noop;

    FB.logout();

    var authorize_url = 'https://www.facebook.com/logout.php?next='+encodeURIComponent(this.my_next)+'&access_token='+FB.getAccessToken()+'&confirm=1';
    var ref = window.open(authorize_url, '_blank', 'location=no');

    ref.addEventListener('loadstop', function(event) {
      if(typeof callback == 'function') callback();
      ref.close();
    });

  }


};